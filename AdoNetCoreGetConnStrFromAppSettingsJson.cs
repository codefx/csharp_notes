﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace AdoNetCore.DAL
{
    public static class AdoNetCoreGetConnStrFromAppSettingsJson
    {
        /// <summary>
        /// this is for DotNetCore 2.1, to use "connection string" at the AdoNet from "appsettings.json"
        /// this gets the "connection string" from "appsettings.json" to "ado.net" for ".NetCore ".
        /// </summary>
        /// <param name="obj">empty</param>
        /// <returns>this returns connection string from configuration file, appsettings.json</returns>
        public static string GetConnStr(this object obj)
        {
            var configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", false)
            .Build();

            string connectionString = configuration.GetSection("ConnectionStrings:Default").Value;

            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("No connection string in config.json");
            return connectionString;
        }
    }
}
