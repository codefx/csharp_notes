﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdoNetTools
{
    public class AdoNetStoredProcedures
    {
        private void CategoryAddStoredProcedure()
        {

            SqlConnection conn = new SqlConnection("Server=.;Database=NORTHWND; User=sa;PWD=:)");
            SqlCommand cmd = new SqlCommand("CategoryAdd", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Name", "Meyva");
            cmd.Parameters.AddWithValue("@Description", "meyveler yenilebilen şeylerdir");
            conn.Open();
            int success = cmd.ExecuteNonQuery();
            conn.Close();

        }
        private void CategoryDeleteStoredProcedure()
        {

            SqlConnection conn = new SqlConnection("Server=.;Database=NORTHWND; User=sa;PWD=:)");
            SqlCommand cmd = new SqlCommand("CategoryDel", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CatID", 9);
            conn.Open();
            int success = cmd.ExecuteNonQuery();
            conn.Close();

        }
        private void CategoryListStoredProcedure()
        {
            SqlDataAdapter adp = new SqlDataAdapter("CategoryList", "Server=.;Database=NORTHWND; User=sa;PWD=:)");
            adp.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            adp.Fill(dt);
        }
        private void CategoryListAddStoredProcedure()
        {
            SqlDataAdapter adp = new SqlDataAdapter("CategoryAdd", "Server=.;Database=NORTHWND; User=sa;PWD=:)");
            adp.SelectCommand.CommandType = CommandType.StoredProcedure;
            adp.SelectCommand.Parameters.AddWithValue("@Name", "Karpuz");
            adp.SelectCommand.Parameters.AddWithValue("@Description", "Karpuz sa yaz meyvesidir");
            
            DataTable dt = new DataTable();
            adp.Fill(dt);
        }
    }
}
