﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AdoNetTools
{
    public class AdoNetDataTableToList
    {
        /// <summary>
        /// to reflect your datatable to list of your model or class  
        /// </summary>
        /// <typeparam name="YourModel"></typeparam>
        /// <param name="sqlCommand">SQL command</param>
        /// <param name="conStr">Connection String</param>
        /// <returns>returns list of your Model or what you wish table-class</returns>
        public List<YourModel> FillViewModel<YourModel>(string sqlCommand, string conStr) where YourModel : class, new()
        {
            SqlDataAdapter da = new SqlDataAdapter(sqlCommand, conStr);
            DataTable dt = new DataTable();
            da.Fill(dt);         
            return ConvertDataTableToList<YourModel>(dt);
        }
        /// <summary>
        /// only convert datatable to list
        /// </summary>
        /// <typeparam name="YourModel"></typeparam>
        /// <param name="dt">to convert datatable</param>
        /// <returns>returns list of your model</returns>
        public List<YourModel> ConvertDataTableToList<YourModel>(DataTable dt) where YourModel : class, new()
        {

            var ListT = new List<YourModel>();

            foreach (var row in dt.AsEnumerable())
            {
                var tdn = new YourModel();
                foreach (var prop in tdn.GetType().GetProperties())
                {
                    try
                    {
                        PropertyInfo propertyInfo = tdn.GetType().GetProperty(prop.Name);
                        propertyInfo.SetValue(tdn, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                    }
                    catch
                    {
                        continue;
                    }
                }
                ListT.Add(tdn);
            }
            return ListT;
        }        
    }
    // if you wish an extension method !!! this is quoted from "https://stackoverflow.com/questions/17107220/convert-dataset-to-list" page
    /// <summary>
    /// this helper is an extension method for convert a datatable to list of your model-class
    /// </summary>
    public static class Helper
    {
        public static List<T> DataTableToList<T>(this DataTable table) where T : class, new()
        {
            try
            {
                List<T> list = new List<T>();

                foreach (var row in table.AsEnumerable())
                {
                    T obj = new T();

                    foreach (var prop in obj.GetType().GetProperties())
                    {
                        try
                        {
                            PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                            propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                        }
                        catch
                        {
                            continue;
                        }
                    }
                    list.Add(obj);
                }
                return list;
            }
            catch
            {
                return null;
            }
        }
    }
    
     //an other format !!!
    public class tx
    {
        public List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        private T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
    }
}
